package com.desafioIndra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desafioIndra.models.User;

public interface UserRepository extends JpaRepository<User, Long> {


}
