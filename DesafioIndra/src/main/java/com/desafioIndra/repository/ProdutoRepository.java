package com.desafioIndra.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.desafioIndra.models.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}

