package com.desafioIndra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desafioIndra.models.Historico;

public interface HistoricoRepository extends JpaRepository<Historico, Long> {


}
