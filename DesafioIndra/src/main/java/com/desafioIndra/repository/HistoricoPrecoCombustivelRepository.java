package com.desafioIndra.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.desafioIndra.models.HistoricoPrecoCombustivelModels;

public interface HistoricoPrecoCombustivelRepository extends JpaRepository<HistoricoPrecoCombustivelModels, Long> {

	Optional<HistoricoPrecoCombustivelModels> findById(long id);

	@Query(nativeQuery = true, value = "select hc.MUNICIPIO, round(avg(hc.VALOR_VENDA) , 2) from HISTORICO_COMBUSTIVEL hc group by hc.MUNICIPIO")
	List<?> getMediaDePrecoDeVendaPorMunicipio();

	@Query(nativeQuery = true, value = "select hc.REGIAO, hc.bandeira, hc.COD_PRODUTO ,hc.DATA_COLETA, hc.ESTADO, hc.MUNICIPIO, hc.NOME_PRODUTO, hc.REVENDA_INSTALACAO, hc.UNIDADE_MEDIDA,\r\n"
			+ "hc.VALOR_COMPRA, hc.VALOR_VENDA from HISTORICO_COMBUSTIVEL hc\r\n"
			+ "group by hc.REGIAO,hc.bandeira, hc.COD_PRODUTO ,hc.DATA_COLETA, hc.ESTADO, hc.MUNICIPIO, hc.NOME_PRODUTO, hc.REGIAO, hc.REVENDA_INSTALACAO, hc.UNIDADE_MEDIDA,\r\n"
			+ "hc.VALOR_COMPRA, hc.VALOR_VENDA")
	List<?> getInformacaoImportadasSiglas();

	@Query(nativeQuery = true, value = "select hc.bandeira, hc.cod_produto, hc.data_coleta, hc.estado, hc.municipio, hc.nome_produto, hc.regiao, hc.revenda_instalacao, hc.unidade_medida, hc.valor_compra, hc.valor_venda from historico_combustivel hc\r\n"
			+ "group by hc.bandeira, hc.cod_produto, hc.data_coleta, hc.estado, hc.municipio, hc.nome_produto, hc.regiao, hc.revenda_instalacao, hc.unidade_medida, hc.valor_compra, hc.valor_venda ")
	List<?> getInformaçõesImportadasPorDistribuidora();

	@Query(nativeQuery = true, value = "select hc.data_coleta, hc.cod_produto, hc.bandeira , hc.estado, hc.municipio, hc.nome_produto, hc.regiao, hc.revenda_instalacao, hc.unidade_medida, "
			+ " hc.valor_compra, hc.valor_venda from historico_combustivel hc\r\n"
			+ " group by  hc.data_coleta, hc.cod_produto,hc.bandeira, hc.estado, hc.municipio, hc.nome_produto, "
			+ " hc.regiao, hc.revenda_instalacao, hc.unidade_medida, hc.valor_compra, hc.valor_venda  ")
	List<?> getInformaçõesAgrupadasPorData();

	@Query(nativeQuery = true, value = " select hc.MUNICIPIO, round(avg(hc.VALOR_VENDA) , 2) from HISTORICO_COMBUSTIVEL hc group by hc.MUNICIPIO  ")
	List<?> getMediaDePrecoDeCompraPorMunicipio();

	@Query(nativeQuery = true, value = "select hc.BANDEIRA, round(avg(hc.VALOR_COMPRA) , 2) ,  round(avg(hc.VALOR_VENDA) , 2) from HISTORICO_COMBUSTIVEL hc group by  hc.BANDEIRA ")
	List<?> getMediaPrecoCompraVendaPorDistribuidora();

}
