package com.desafioIndra.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.desafioIndra.models.Revendedor;

public interface RevendedorRepository extends JpaRepository<Revendedor, Long> {

}


