package com.desafioIndra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desafioIndra.models.Localizacao;

public interface LocalizacaoRepository extends JpaRepository<Localizacao, Long> {


}
