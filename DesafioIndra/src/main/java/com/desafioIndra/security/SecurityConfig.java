package com.desafioIndra.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;

import com.desafioIndra.config.ConfiguracaoSwaggerConfig;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import(ConfiguracaoSwaggerConfig.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		CorsConfiguration cors = new CorsConfiguration();
		cors.addAllowedOrigin("*");
		cors.addAllowedMethod("*");
		cors.addAllowedHeader("*");
		http.cors().configurationSource(request -> cors).and().csrf().disable().authorizeRequests()
				.antMatchers("/*/swagger-ui/**", "/*/swagger-ui.html/**", "/swagger-ui.html", "/*/h2-console/**",
						"/h2-console/**", "/v2/api-docs", "/configuration/ui", "/swagger-resources",
						"/configuration/security", "/swagger-ui.html", "/webjars/**")
				.permitAll().antMatchers("/*/historicos/**").hasRole("USER").antMatchers("/*/usuarios/**")
				.hasRole("ADMIN").anyRequest().authenticated().and().httpBasic();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("teste").password("{noop}123").roles("USER").and().withUser("admin")
				.password("{noop}123").roles("USER", "ADMIN");
	}

	
	  @Override public void configure(WebSecurity web) throws Exception {
	  web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui",
	  "/swagger-resources", "/configuration/security", "/swagger-ui.html",
	  "/webjars/**"); }
	 

}