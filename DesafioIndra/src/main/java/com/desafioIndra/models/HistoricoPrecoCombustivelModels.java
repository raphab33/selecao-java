package com.desafioIndra.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "historico_combustivel")
public class HistoricoPrecoCombustivelModels implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String regiao;

	private String estado;

	private String municipio;

	private String revendaInstalacao;

	private long codProduto;

	private String nomeProduto;

	private String dataColeta;

	private Double valorCompra;

	private Double valorVenda;

	private String unidadeMedida;

	private String bandeira;

}