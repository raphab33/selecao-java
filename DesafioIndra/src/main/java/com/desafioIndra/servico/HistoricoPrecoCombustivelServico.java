package com.desafioIndra.servico;

import java.io.BufferedReader;
import java.io.FileReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desafioIndra.models.HistoricoPrecoCombustivelModels;
import com.desafioIndra.repository.HistoricoPrecoCombustivelRepository;

@Service
public class HistoricoPrecoCombustivelServico {

	@Autowired
	private HistoricoPrecoCombustivelRepository csvCombustivel;

	public void salvarDadosDoHistorico() {

		String linha = "";
		boolean primeiraLinha = true;
		try {
			BufferedReader br = new BufferedReader(new FileReader("src/main/resources/2018-1_CA.csv"));
			while ((linha = br.readLine()) != null) {
				String[] dados = linha.split("  ");

				if (primeiraLinha) {
					primeiraLinha = false;
					continue;
				}
				HistoricoPrecoCombustivelModels hpc = new HistoricoPrecoCombustivelModels();
				hpc.setRegiao(dados[0]);
				hpc.setEstado(dados[1]);
				hpc.setMunicipio(dados[2]);
				hpc.setRevendaInstalacao(dados[3]);
				hpc.setCodProduto(Long.parseLong(dados[4]));
				hpc.setNomeProduto(dados[5]);
				hpc.setDataColeta(dados[6]);
				if (("".equals(dados[7]) || null == dados[7])) {
					hpc.setValorCompra(0.0);
				} else {
					dados[7] = dados[7].replace(',', '.');
					hpc.setValorCompra(Double.parseDouble(dados[7]));
				}
				if (("".equals(dados[8]) || null == dados[8])) {
					hpc.setValorVenda(0.0);
				} else {
					dados[8] = dados[8].replace(',', '.');
					hpc.setValorVenda(Double.parseDouble(dados[8]));
				}
				hpc.setUnidadeMedida(dados[9]);
				hpc.setBandeira(dados[10]);

				csvCombustivel.save(hpc);
			}
			br.close();
		} catch (Exception e) {
			System.out.println("Erro: " + e);
		}
	}

	/*
	 * public List<HistoricoPrecoCombustivelModels> primeiraConsulta(long
	 * cod_produto) { return csvCombustivel.mediaDePrecoPorMunicipio(); }
	 */
}
