package com.desafioIndra.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafioIndra.models.HistoricoPrecoCombustivelModels;
import com.desafioIndra.repository.HistoricoPrecoCombustivelRepository;
import com.desafioIndra.servico.HistoricoPrecoCombustivelServico;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API REST Historicos")
@CrossOrigin(origins = "*")
public class HistoricoPrecoCombustivelController {

	@Autowired
	private HistoricoPrecoCombustivelServico historicoPrecoCombustivelServico;

	@Autowired
	private HistoricoPrecoCombustivelRepository historicoPrecoCombustivelRepository;

	@ApiOperation(value = "Importar arquivo CSV")
	@GetMapping(path = "/importarCSV")
	public void setDadosInDB() {
		historicoPrecoCombustivelServico.salvarDadosDoHistorico();
	}

	@ApiOperation(value = "Retorna Media de Preco de venda Por Municipio")
	@GetMapping(path = "/mediaDePrecoDeVendaPorMunicipios")
	public ResponseEntity<?> mediaPrecoVendaPorMunicipio() {
		return new ResponseEntity<>(historicoPrecoCombustivelRepository.getMediaDePrecoDeVendaPorMunicipio(),
				HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna todas as informacoes agrupadas por distribuidora")
	@GetMapping(path = "/informacoesAgrupadasPorDistribuidora")
	public ResponseEntity<?> informacoesAgrupadasPorDistribuidora() {
		return new ResponseEntity<>(historicoPrecoCombustivelRepository.getInformaçõesImportadasPorDistribuidora(),
				HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna todas as informacoes agrupadas por data")
	@GetMapping(path = "/informacoesAgrupadasPorData")
	public ResponseEntity<?> informacoesImportadasPorData() {
		return new ResponseEntity<>(historicoPrecoCombustivelRepository.getInformaçõesAgrupadasPorData(),
				HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna Media de Preco de venda Por Municipio")
	@GetMapping(path = "/mediaDePrecoDeCompraPorMunicipios")
	public ResponseEntity<?> mediaPrecoCompraPorMunicipio() {
		return new ResponseEntity<>(historicoPrecoCombustivelRepository.getMediaDePrecoDeCompraPorMunicipio(),
				HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna Media de Preco de compra e venda Por Municipio")
	@GetMapping(path = "/mediaDePrecoCompraVendaPorMunicipios")
	public ResponseEntity<?> mediaPrecoCompraVendaPorMunicipio() {
		return new ResponseEntity<>(historicoPrecoCombustivelRepository.getMediaPrecoCompraVendaPorDistribuidora(),
				HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna todas as informacoes agrupadas regiao")
	@GetMapping(path = "/informacoesImportadasPorSiglaRegiao")
	public ResponseEntity<?> InformacaoImportadasSiglas() {
		return new ResponseEntity<>(historicoPrecoCombustivelRepository.getInformacaoImportadasSiglas(), HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna unico historico pelo ID")
	@GetMapping("/historicoss/{id}")
	public Optional<HistoricoPrecoCombustivelModels> listaHistoricoUnico(@PathVariable(value = "id") long id) {
		return historicoPrecoCombustivelRepository.findById(id);
	}

	@ApiOperation(value = "Salva um historico")
	@PostMapping("/historicoss")
	public HistoricoPrecoCombustivelModels salvaHistorico(
			@RequestBody @Valid HistoricoPrecoCombustivelModels historico) {
		return historicoPrecoCombustivelRepository.save(historico);
	}

	@ApiOperation(value = "Deleta um historico")
	@DeleteMapping("/historicoss")
	public void deletaHistorico(@RequestBody @Valid HistoricoPrecoCombustivelModels historico) {
		historicoPrecoCombustivelRepository.delete(historico);
	}

	@ApiOperation(value = "Atualiza um historico")
	@PutMapping("/historicoss")
	public HistoricoPrecoCombustivelModels atualizaUsuario(
			@RequestBody @Valid HistoricoPrecoCombustivelModels historico) {
		return historicoPrecoCombustivelRepository.save(historico);
	}

}
