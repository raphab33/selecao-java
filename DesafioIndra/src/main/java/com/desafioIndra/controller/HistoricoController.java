package com.desafioIndra.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafioIndra.models.Historico;
import com.desafioIndra.repository.HistoricoRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API REST Historicos")
@CrossOrigin(origins = "*")
public class HistoricoController {
	
	@Autowired
	HistoricoRepository historicos;

	@ApiOperation(value = "Retorna uma lista de historicos")
	@GetMapping("/historicos")
	public List<Historico> listHistoricos() {
		return historicos.findAll();

	}
	
	@ApiOperation(value = "Retorna unico usuário pelo ID")
	@GetMapping("/historicos/{id}")
	public Optional<Historico> listHistoricobyId(@PathVariable(value = "id") long id) {
		return historicos.findById(id);
	}

	@ApiOperation(value = "Salva um historico")
	@PostMapping("/historicos")
	public Historico postHistorico(@RequestBody @Valid Historico historico) {
		return historicos.save(historico);
	}

	@ApiOperation(value = "Deleta um usuario")
	@DeleteMapping("/historicos")
	public void deleteUser(@RequestBody @Valid Historico historico) {
		historicos.delete(historico);
	}

	@ApiOperation(value = "Atualiza um usuario")
	@PutMapping("/historicos")
	public Historico updateUser(@RequestBody @Valid Historico historico) {
		return historicos.save(historico);
	}

}
