package com.desafioIndra.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.desafioIndra.models.User;
import com.desafioIndra.repository.UserRepository;

import io.swagger.annotations.ApiOperation;

@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class UserResource {

	@Autowired
	UserRepository users;

	@ApiOperation(value = "Retorna uma lista de Usuarios")
	@GetMapping("/users")
	public List<User> listUsers() {
		return users.findAll();

	}
	
	@ApiOperation(value = "Retorna unico usuário pelo ID")
	@GetMapping("/users/{id}")
	public Optional<User> listUserbyId(@PathVariable(value = "id") long id) {
		return users.findById(id);
	}

	@ApiOperation(value = "Salva um usuario")
	@PostMapping("/users")
	public User postUser(@RequestBody @Valid User user) {
		return users.save(user);
	}

	@ApiOperation(value = "Deleta um usuario")
	@DeleteMapping("/users")
	public void deleteUser(@RequestBody @Valid User user) {
		users.delete(user);
	}

	@ApiOperation(value = "Atualiza um usuario")
	@PutMapping("/users")
	public User updateUser(@RequestBody @Valid User user) {
		return users.save(user);
	}

}
